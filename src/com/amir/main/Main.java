package com.amir.main;

import org.quartz.SchedulerException;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.amir.timer.dao.TimerDao;
import com.amir.timer.dao.TimerDaoImpl;
import com.amir.timer.util.RootApplicationContextProvider;

//@SpringBootApplication
public class Main {

	public static void main(String[] args) throws SchedulerException, InterruptedException {
		ApplicationContext ap = /*RootApplicationContextProvider.getApplicationContext();*/new ClassPathXmlApplicationContext("ApplicationContext.xml");
		TimerDaoImpl td = (TimerDaoImpl)ap.getBean("timerDao");
		td.getStatus();

	}

}
