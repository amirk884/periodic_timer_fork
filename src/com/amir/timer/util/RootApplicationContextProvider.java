package com.amir.timer.util;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.amir.timer.dao.TimerDaoImpl;

@Component
public class RootApplicationContextProvider implements ApplicationContextAware {
	
	private static ApplicationContext applicationContext=null;

	public static ApplicationContext getApplicationContext() {
		return applicationContext;
	}

	public  void setApplicationContext(ApplicationContext applicationContext) {
		RootApplicationContextProvider.applicationContext = applicationContext;
	}
	
	

}
