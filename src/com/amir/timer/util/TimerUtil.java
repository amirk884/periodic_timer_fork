package com.amir.timer.util;

import javax.jms.ConnectionFactory;
import javax.jms.Queue;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.springframework.beans.factory.annotation.Value;

public class TimerUtil {
	
	private  ConnectionFactory connectionFactory=null;
	private  Queue queue=null;
	private Context initContext =null;
	
	@Value("${timer.connectionfactory.jndiname}")
	private String connectionFactoryName;
	
	@Value("${timer.queue.jndiname=}")
	private String queueName;
	
	public TimerUtil() {
		try {
			initContext = new InitialContext();
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public  ConnectionFactory getConnectionFactory() {
		return connectionFactory;
	}

	public void setConnectionFactory() {
		try {
			connectionFactory = (ConnectionFactory)initContext.lookup(connectionFactoryName);
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public  javax.jms.Queue getQueue() {
		return queue;
	}

	public void setQueue() {
		try {
			queue = (Queue)initContext.lookup(queueName);
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


}
