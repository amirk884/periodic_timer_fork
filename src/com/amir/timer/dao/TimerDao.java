package com.amir.timer.dao;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SchedulerException;

import com.amir.timer.dto.TimerDTO;

public interface TimerDao {

	void runTimer(TimerDTO timerDTO) throws SchedulerException,InterruptedException;
}
