package com.amir.timer.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
//import java.util.Date;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.quartz.CronScheduleBuilder;
import org.quartz.CronTrigger;
//import org.quartz.DateBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.amir.timer.dto.TimerDTO;
import com.amir.timer.job.TimerJob;

@Repository("timerDao")
public class TimerDaoImpl implements TimerDao {
	
	private JdbcTemplate jdbcTemplate;
	private static final Logger logger = Logger.getLogger(TimerDaoImpl.class);
	
	@Value("${timer.selectEvent}")
	private String selectEventQuery;
	
	@Autowired
	public void setDataSource(DataSource dataSource) {
		jdbcTemplate = new JdbcTemplate(dataSource);
	}
	@PostConstruct
	public void getStatus() throws SchedulerException, InterruptedException {
		//BasicConfigurator.configure();
		logger.info("getStatus started");
		TimerDTO td = jdbcTemplate.queryForObject(selectEventQuery,new RowMapper<TimerDTO>() {

			@Override
			public TimerDTO mapRow(ResultSet rs, int rownum) throws SQLException {
				TimerDTO timerDTO = new TimerDTO();
				timerDTO.setName(rs.getString(1));
				timerDTO.setCron(rs.getString(2));
				timerDTO.setEventCode(rs.getString(3));
				timerDTO.setEnabled(rs.getString(4));
				logger.debug("getStatus completed , event code "+timerDTO.getEventCode());
				return timerDTO;
			}
		});
		
		runTimer(td);
	}

	public void runTimer(TimerDTO timerDTO) throws SchedulerException, InterruptedException {
		SchedulerFactory sf = new StdSchedulerFactory();
		Scheduler sched = sf.getScheduler();
		
		JobDetail job = JobBuilder.newJob(TimerJob.class).withIdentity("job1","group1").build();
		
		//Date runTime = DateBuilder.evenMinuteDate(new Date());
		String cron = timerDTO.getCron();
		
		CronTrigger trigger = TriggerBuilder.newTrigger()
				.withIdentity("trigger1","group1")
				//.startAt(runTime)
				.withSchedule(CronScheduleBuilder.cronSchedule(cron))
				.build();
		
		sched.scheduleJob(job,trigger);
		
		sched.start();
		logger.debug("scheduler  started, cron exp " + timerDTO.getCron());
		//Thread.sleep(90L*1000L);
		
		//sched.shutdown();
		
	}

	

}
