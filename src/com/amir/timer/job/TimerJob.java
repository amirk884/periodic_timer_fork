package com.amir.timer.job;

import java.util.Date;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;

import com.amir.timer.dao.TimerDaoImpl;
import com.amir.timer.util.TimerUtil;

public class TimerJob implements Job {
	
	private static final Logger logger = Logger.getLogger(TimerJob.class);
	
	public void execute(JobExecutionContext context) {
		logger.debug("job started");
		System.out.println("Hello Amir! " + new Date());
		TimerUtil timerUtil = new TimerUtil();
		/*ConnectionFactory connectionFactory=timerUtil.getConnectionFactory();
		javax.jms.Queue queue=timerUtil.getQueue();
		MessageProducer producer =null;
		
		try {
			Connection conn = connectionFactory.createConnection();
			Session session = conn.createSession(false,Session.AUTO_ACKNOWLEDGE);
			producer = session.createProducer(queue);
			ObjectMessage msg = session.createObjectMessage();
			msg.setObject(new Date());
			producer.send(msg);
		} catch (JMSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		logger.debug("job completed");
	}

}
